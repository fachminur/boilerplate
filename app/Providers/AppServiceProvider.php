<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\Models\Rmenu;
use App\Models\TroleAccess;
use Illuminate\Support\Facades\Schema;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        View::composer('*', function($view)
        {
            if (Auth::check()){
                // dd($view->getName());
                $menus = TroleAccess::leftJoin('r_menu', function($join) {
                        $join->on('t_role_access.idr_menu', '=', 'r_menu.idr_menu');
                    })
                    ->where('status_menu', '1')
                    ->where('read_access', '1')
                    ->where('t_role_access.idr_role', Auth::user()->idr_role)
                    ->where('idr_parent',null)
                    ->orderBy('order_menu')
                    ->get();
                foreach($menus as $r){
                    $child = TroleAccess::leftJoin('r_menu', function($join) {
                            $join->on('t_role_access.idr_menu', '=', 'r_menu.idr_menu');
                        })
                        ->where('status_menu', '1')
                        ->where('read_access', '1')
                        ->where('t_role_access.idr_role', Auth::user()->idr_role)
                        ->where('idr_parent',$r->idr_menu)
                        ->orderBy('order_menu')
                        ->get();
                    $r->child = $child;
                }
                // dd($menus);
                $view->with('menus', $menus);
            }else{
                $view->with('menus', null);
            }
        });
    }
}
