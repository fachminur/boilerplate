<?php

namespace App\Http\Controllers;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Rrole;

class RoleController extends Controller
{
    protected $route_name = 'role';
    protected $route_detail = 'roleaccess';

    public function __construct(Rrole $role)
    {
        $this->middleware('auth');
        $this->role = $role;
        //get parmission menu where kode menu and status true
        $this->menu = getPermissionMenu('KDROLE');
        $var = array(
            'title' => 'Role',
            'route_name' => 'role'
        );
        $breadcrumb = [
            [
                "route" => 'home',
                "name" => "Home"
            ],
            [
                "route" => 'role',
                "name" => "Role"
            ]
        ];
        View::share(['var'=> $var, 'permissionMenu' => $this->menu, 'breadcrumb' => $breadcrumb]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->role->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('role_name',function($data){
                    if (empty($data->role_name)) {
                        $role_name = "<p class='text-danger'>Tidak ada Nama Role</p>";
                    } else {
                        $role_name = $data->role_name;
                    }
                    return $role_name;
                })

                ->addColumn('role_code',function($data){
                    if (empty($data->role_code)) {
                        $role_code = "<p class='text-danger'>Tidak ada Nama Role</p>";
                    } else {
                        $role_code = $data->role_code;
                    }
                    return $role_code;
                })
                
                ->addColumn('role_status',function($data){
                    if ($data->role_status == 1) {
                        $role_status = "<div class='badge badge-success'>Active</div>";
                    } else {
                        $role_status = "<div class='badge badge-danger'>Inactive</div>";
                    }
                    return $role_status;
                })
                
                ->addColumn('action', function($data){
                    $btn = '
                        <a href="'.url($this->route_name.'/detail-role/'.$data->idr_role).'" class="btn btn-icon btn-info my-1" title="Edit"><i class="fas fa-search"></i></a>
                        <a href="'.route($this->route_name.'.edit',$data->idr_role).'" class="btn btn-icon btn-warning my-1" title="Edit"><i class="fas fa-edit"></i></a>
                        <a href="'.route($this->route_name.'.destroy', $data->idr_role).'" data-id="" class="btn btn-icon btn-danger btn-delete-on-table" title="Delete"><i class="fas fa-trash"></i></a>
                        ';
                    return $btn;
                })
                ->rawColumns(['action','role_status','role_name','role_code'])
                ->make(true);
        }

        return view($this->route_name.'.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = route($this->route_name.".store");
        return view($this->route_name.'.form', compact('action'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'role_name' => 'required',
            'role_code' => 'required',
            'role_status' => 'required',
        ],[
            'required'  => 'Kolom :attribute tidak boleh kosong.',
            'unique'    => ':attribute sudah digunakan'
        ]);
        
        $role = new $this->role;
        $role->role_name = $request->role_name;
        $role->role_code = $request->role_code;
        $role->role_status = $request->role_status;
        $role->created_by = Auth::user()->name;
        $role->created_at = new \DateTime;

        $role->save();
        return redirect($this->route_name)->with(['success' => 'Data berhasil disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $action = route($this->route_name.'.update',$id);
        $data = $this->role->findOrFail($id);
        return view($this->route_name.'.form', compact('data', 'action'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'role_name' => 'required',
            'role_code' => 'required',
            'role_status' => 'required',
        ],[
            'required'  => 'Kolom :attribute tidak boleh kosong.',
            'unique'    => ':attribute sudah digunakan'
        ]);
        
        $role = $this->role->findOrFail($id);

        $role->role_name = $request->role_name;
        $role->role_code = $request->role_code;
        $role->role_status = $request->role_status;
        $role->updated_by = Auth::user()->name;
        $role->updated_at = new \DateTime;

        $role->save();
        return redirect($this->route_name)->with(['success' => 'Data berhasil diperbarui!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = $this->role->findOrFail($id);
        if($delete->delete()){
            return response()->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()->json(['code' => '400', 'message' => 'Data gagal dihapus']);
        }
    }
}
