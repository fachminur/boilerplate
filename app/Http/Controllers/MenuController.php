<?php

namespace App\Http\Controllers;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\Models\Rmenu;
use Illuminate\Support\Facades\Auth;
use DB;

class MenuController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    
    public function __construct(Rmenu $rmenu)
    {
        $this->middleware('auth');
        //array permission menu
        $this->position_menu = ['SIDE','TOP','CONTENT','TITLE','SIDEPARENT','SIDECHILD'];
        //get parmission menu where kode menu and status true
        $this->menu = getPermissionMenu('MNU');
        //declare breadcrumb link
        $breadcrumb = [
            [
                "route" => 'home',
                "name" => "Home"
            ],
            [
                "route" => 'menu',
                "name" => "Menu"
            ]
        ];
        //declare array
        $var = array(
            'title' => 'Menu',
            'route_name' => 'menu'
        );
        View::share(['var' => $var, 'permissionMenu' => $this->menu, 'breadcrumb' => $breadcrumb]);
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Rmenu::orderBy('order_menu', 'ASC')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('parent',function($data){
                    if (empty($data->idr_parent)) {
                        $parent = "<p class='text-danger'>Tidak ada Parent</p>";
                    } else {
                        $parent = $data->idr_parent;
                    }
                    return $parent;
                })
                ->addColumn('route',function($data){
                    if (empty($data->controller_menu)) {
                        $route = "<p class='text-danger'>Tidak ada Route</p>";
                    } else {
                        $route = $data->controller_menu;
                    }
                    return $route;
                })
                ->addColumn('icon',function($data){
                    if (empty($data->icon_menu)) {
                        $icon = "<p class='text-danger'>Tidak ada Icon</p>";
                    } else {
                        $icon = $data->icon_menu;
                    }
                    return $icon;
                })
                ->addColumn('status',function($data){
                    if ($data->status_menu == 1) {
                        $status = "<div class='badge badge-success'>Active</div>";
                    } else {
                        $status = "<div class='badge badge-danger'>Inactive</div>";
                    }
                    return $status;
                })
                ->addColumn('action', function($data){
                    $up = '<a href="'.route('menu.up').'" class="btn btn-icon btn-light mr-1 mb-1 btn-update-on-table" title="Up Menu"><i class="fas fa-arrow-up"></i></a>';
                    $down = '<a href="'.route('menu.down').'" class="btn btn-icon btn-light mr-1 mb-1 btn-update-on-table" title="Down Menu"><i class="fas fa-arrow-down"></i></a>';
                    $edit = '<a href="'.route('menu.edit',$data->idr_menu).'" class="btn btn-icon btn-warning mr-1 mb-1" title="Edit"><i class="fas fa-edit"></i></a>';
                    $delete = '<a href="'.route('menu.destroy', $data->idr_menu).'" data-id="" class="btn btn-icon btn-danger mr-1 mb-1 btn-delete-on-table" title="Delete"><i class="fas fa-trash"></i></a>';
                    if($this->menu['edit_access'] == '0' && $this->menu['delete_access'] == '0'){                            
                        $btn = $up.$down;
                    //permission edit true, permission delete false
                    }else if($this->menu['edit_access'] == '1' && $this->menu['delete_access'] == '0'){
                        $btn = $up.$down.$edit;
                    //permission edit true, permission delete false
                    }else if($this->menu['edit_access'] == '0' && $this->menu['delete_access'] == '1'){
                        $btn = $up.$down.$delete;
                    //permission edit true, permission delete true
                    }else if($this->menu['edit_access'] == '1' && $this->menu['delete_access'] == '1'){
                        $btn = $up.$down.$edit.$delete;
                    }
                    return $btn;
                })
                ->rawColumns(['action','status','read','add','edit','delete','parent','route','icon'])
                ->make(true);
        }
        return view('menu.index');
    }

    public function create(){
        $action = route('menu.store');
        $menu_parent = $rmenu::where('position_menu', 'SIDEPARENT')->get();

        return view('menu.add')->with(['action' => $action, 'position' => $this->position_menu, 'menu' => $menu_parent]);
    }

    public function store(Request $request){
        $validated = $request->validate([
            'name_menu' => 'required|unique:r_menu',
            'controller_menu' => 'required',
            'kode_menu' => 'required',
            'position_menu' => 'required',
        ],[
            'required'  => 'Kolom :attribute tidak boleh kosong.',
            'unique'    => ':attribute sudah digunakan'
        ]);

        //get last order number
        $order_number = Rmenu::orderBy('order_menu', 'DESC')->first();

        $save = new Rmenu;
        $save->name_menu = $request->name_menu;
        $save->controller_menu = $request->controller_menu;
        $save->kode_menu = $request->kode_menu;
        $save->position_menu = $request->position_menu;
        $save->idr_parent = $request->idr_parent;
        $save->icon_menu = $request->icon_menu;
        $save->status_menu = $request->status;
        $save->order_menu = $order_number->order_menu+1;
        $save->created_by = Auth::user()->name;
        $save->save();

        return redirect('menu')->with(['success' => 'Data berhasil disimpan!']);
    }

    public function edit($id)
    {
        $data = Rmenu::findOrFail($id);
        $action = route('menu.update',$id);
        $menu_parent = $rmenu::where('position_menu', 'SIDEPARENT')->get();
        
        return view('menu.add', compact('data','action'))->with(['position' => $this->position_menu, 'menu' => $menu_parent]);
    }

    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name_menu' => 'required',
            'controller_menu' => 'required',
            'kode_menu' => 'required',
            'position_menu' => 'required',
        ],[
            'required'  => 'Kolom :attribute tidak boleh kosong.',
            'unique'    => ':attribute sudah digunakan'
        ]);

        $update = Rmenu::findOrFail($id);
        $update->name_menu = $request->name_menu;
        $update->controller_menu = $request->controller_menu;
        $update->kode_menu = $request->kode_menu;
        $update->position_menu = $request->position_menu;
        $update->idr_parent = $request->idr_parent;
        $update->icon_menu = $request->icon_menu;
        $update->status_menu = $request->status;
        $update->updated_by = Auth::user()->name;
        $update->save();

        return redirect('menu')->with(['success' => 'Data berhasil diperbarui!']);
    }

    public function destroy($id){
        $delete = Rmenu::findOrFail($id);
        if($delete->delete()){
            return response()->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()->json(['code' => '400', 'message' => 'Data gagal dihapus']);
        }
    }

    public function up(Request $request){
        $id = $request->post('id');
        $value = $request->post('value');
        
        if($request->post('value') == 1){
            return response()->json(['code' => '400', 'message' => 'Urutan menu sudah pertama']);
        }

        DB::beginTransaction();
        try {
            //mendapatkan row menu sebelumnya
            $menu_prev = Rmenu::where('order_menu', $value-1)->first();
            $menu_prev->order_menu = $value;
            $menu_prev->save();

            //mendapatkan row menu selanjutnya
            $menu_next = Rmenu::findOrFail($id);
            $menu_next->order_menu = $value-1;
            $menu_next->save();
            DB::commit();
            return response()->json(['code' => '200', 'message' => 'Data berhasil diperbarui</br>Silahkan Refresh page']);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['code' => '400', 'message' => 'Data gagal diperbarui']);
        }
    }

    public function down(Request $request){
        $id = $request->post('id');
        $value = $request->post('value');

        DB::beginTransaction();
        try {
            //mendapatkan row menu sebelumnya
            $menu_prev = Rmenu::where('order_menu', $value+1)->first();
            $menu_prev->order_menu = $value;
            $menu_prev->save();

            //mendapatkan row menu selanjutnya
            $menu_next = Rmenu::findOrFail($id);
            $menu_next->order_menu = $value+1;
            $menu_next->save();
            DB::commit();
            return response()->json(['code' => '200', 'message' => 'Data berhasil diperbarui</br>Silahkan Refresh page']);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['code' => '400', 'message' => 'Data gagal diperbarui']);
        }
    }
}
