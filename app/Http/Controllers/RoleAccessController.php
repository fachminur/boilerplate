<?php

namespace App\Http\Controllers;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\TroleAccess;
use App\Models\Rrole;
use App\Models\Rmenu;

class RoleAccessController extends Controller
{
    protected $route_name = 'roleaccess';
    protected $route_parent = 'role/detail-role';

    public function __construct(TroleAccess $role_access, Rrole $role, Rmenu $menu)
    {
        $this->middleware('auth');
        $this->role = $role;
        $this->menu = $menu;
        $this->role_access = $role_access;
        $breadcrumb = [
            [
                "route" => 'home',
                "name" => "Home"
            ],
            [
                "route" => 'role',
                "name" => "Role"
            ]
        ];
        $var = array(
            'title' => 'Role Access',
            'route_name' => 'roleaccess',
            'route_parent' => 'role/detail-role'
        );

        View::share(['var'=> $var, 'breadcrumb' => $breadcrumb]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index(Request $request)
    // {
    //     $role = session()->get('role');
    //     if ($request->ajax()) {
    //         $data = $this->role_access->where('idr_role', $role->idr_role)->latest()->get();
    //         return Datatables::of($data)
    //             ->addIndexColumn()
    //             ->addColumn('role',function($data){
    //                 if (empty($data->role->role_name)) {
    //                     $role = "<p class='text-danger'>Tidak ada Role</p>";
    //                 } else {
    //                     $role = $data->role->role_name;
    //                 }
    //                 return $role;
    //             })

    //             ->addColumn('access',function($data){
    //                 if (empty($data->menu->name_menu)) {
    //                     $access = "<p class='text-danger'>Tidak ada Access</p>";
    //                 } else {
    //                     $access = $data->menu->name_menu;
    //                 }
    //                 return $access;
    //             })

    //             ->addColumn('read',function($data){
    //                 if ($data->read_access == 1) {
    //                     $read = "<figure class='avatar avatar-xs bg-success text-white'></figure>";
    //                 } else {
    //                     $read = "<figure class='avatar avatar-xs bg-danger text-white'></figure>";
    //                 }
    //                 return $read;
    //             })
    //             ->addColumn('add',function($data){
    //                 if ($data->add_access == 1) {
    //                     $add = "<figure class='avatar avatar-xs bg-success text-white'></figure>";
    //                 } else {
    //                     $add = "<figure class='avatar avatar-xs bg-danger text-white'></figure>";
    //                 }
    //                 return $add;
    //             })
    //             ->addColumn('edit',function($data){
    //                 if ($data->edit_access == 1) {
    //                     $edit = "<figure class='avatar avatar-xs bg-success text-white'></figure>";
    //                 } else {
    //                     $edit = "<figure class='avatar avatar-xs bg-danger text-white'></figure>";
    //                 }
    //                 return $edit;
    //             })
    //             ->addColumn('delete',function($data){
    //                 if ($data->delete_access == 1) {
    //                     $delete = "<figure class='avatar avatar-xs bg-success text-white'></figure>";
    //                 } else {
    //                     $delete = "<figure class='avatar avatar-xs bg-danger text-white'></figure>";
    //                 }
    //                 return $delete;
    //             })
                
    //             ->addColumn('action', function($data){
    //                 $btn = '
    //                     <a href="'.route($this->route_name.'.edit',$data->idt_role_access).'" class="btn btn-icon btn-warning my-1" title="Edit"><i class="fas fa-edit"></i></a>
    //                     <a href="'.route($this->route_name.'.destroy', $data->idt_role_access).'" data-id="" class="btn btn-icon btn-danger btn-delete-on-table" title="Delete"><i class="fas fa-trash"></i></a>
                        
    //                     ';
    //                 return $btn;
    //             })
    //             ->rawColumns(['action','role','access', 'read', 'add', 'edit', 'delete'])
    //             ->make(true);
    //     }
    //     return view('roleaccess.index');
        
    // }

    public function detailRole(Request $request, $id)
    {
        $idr_role = $id;
        if ($request->ajax()) {
            $data = $this->role_access->where('idr_role', $id)->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('role',function($data){
                    if (empty($data->role->role_name)) {
                        $role = "<p class='text-danger'>Tidak ada Role</p>";
                    } else {
                        $role = $data->role->role_name;
                    }
                    return $role;
                })

                ->addColumn('access',function($data){
                    if (empty($data->menu->name_menu)) {
                        $access = "<p class='text-danger'>Tidak ada Access</p>";
                    } else {
                        $access = $data->menu->name_menu;
                    }
                    return $access;
                })

                ->addColumn('read',function($data){
                    if ($data->read_access == 1) {
                        $read = "<figure class='avatar avatar-xs bg-success text-white'></figure>";
                    } else {
                        $read = "<figure class='avatar avatar-xs bg-danger text-white'></figure>";
                    }
                    return $read;
                })
                ->addColumn('add',function($data){
                    if ($data->add_access == 1) {
                        $add = "<figure class='avatar avatar-xs bg-success text-white'></figure>";
                    } else {
                        $add = "<figure class='avatar avatar-xs bg-danger text-white'></figure>";
                    }
                    return $add;
                })
                ->addColumn('edit',function($data){
                    if ($data->edit_access == 1) {
                        $edit = "<figure class='avatar avatar-xs bg-success text-white'></figure>";
                    } else {
                        $edit = "<figure class='avatar avatar-xs bg-danger text-white'></figure>";
                    }
                    return $edit;
                })
                ->addColumn('delete',function($data){
                    if ($data->delete_access == 1) {
                        $delete = "<figure class='avatar avatar-xs bg-success text-white'></figure>";
                    } else {
                        $delete = "<figure class='avatar avatar-xs bg-danger text-white'></figure>";
                    }
                    return $delete;
                })
                
                ->addColumn('action', function($data){
                    $btn = '
                        <a href="'.url($this->route_parent.'/edit',$data->idt_role_access).'" class="btn btn-icon btn-warning my-1" title="Edit"><i class="fas fa-edit"></i></a>
                        <a href="'.url($this->route_parent.'/destroy', $data->idt_role_access).'" data-id="" class="btn btn-icon btn-danger btn-delete-on-table" title="Delete"><i class="fas fa-trash"></i></a>
                        
                        ';
                    return $btn;
                })
                ->rawColumns(['action','role','access', 'read', 'add', 'edit', 'delete'])
                ->make(true);
        }
        // dd($idr_role);
        return view('roleaccess.index', compact('idr_role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $action = url($this->route_parent."/store", $id);
        $role_list = $this->role->get();
        $menu_list = $this->menu->get();
        $back_btn = url($this->route_parent, $id);
        return view($this->route_name.'.form')
                ->with(['action' => $action,
                        'role' => $role_list,
                        'menu' => $menu_list,
                        'idr_role' => $id,
                        'back_btn' => $back_btn
                ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        // dd($request->all());
        // dd($id);
        $validated = $request->validate([
            'role' => 'required',
            'menu' => 'required',
        ],[
            'required'  => 'Kolom :attribute tidak boleh kosong.',
            'unique'    => ':attribute sudah ada'
        ]);

        $access_double = $this->role_access
                        ->where('idr_role', $id)
                        ->where('idr_menu', $request->menu)
                        ->first();        

        if($access_double) {
            return redirect(url($this->route_parent, $id))->with(['error' => 'Data sudah ada!']);
        } else {
            $role_access = new $this->role_access;
            $role_access->idr_role = $id;
            $role_access->idr_menu = $request->menu;
            $role_access->add_access = ($request->add_access || $request->add_access == "0")?1:0;
            $role_access->edit_access = ($request->edit_access || $request->edit_access == "0")?1:0;
            $role_access->read_access = ($request->read_access || $request->read_access == "0")?1:0;
            $role_access->delete_access = ($request->delete_access || $request->delete_access == "0")?1:0;
            $role_access->created_by = Auth::user()->name;
            $role_access->created_at = new \DateTime;

            $role_access->save();

            return redirect(url($this->route_parent, $id))->with(['success' => 'Data berhasil disimpan!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->role_access->findOrFail($id);
        $action = url($this->route_parent."/update", $id);
        // $action = route($this->route_name.'.update',$id);
        $role_list = $this->role->get();
        $menu_list = $this->menu->get();
        $roleaccess = $this->role_access->get();
        $back_btn = url($this->route_parent, $data->idr_role);
        return view($this->route_name.'.form')
                ->with([
                    'data' => $data,
                    'action' => $action,
                    'roleaccess' => $roleaccess,
                    'role' => $role_list,
                    'menu' => $menu_list,
                    'idr_role' => $id,
                    'back_btn' => $back_btn
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role_access = $this->role_access->findOrFail($id);
        
        $role_access->add_access = ($request->add_access || $request->add_access == "0")?1:0;
        $role_access->edit_access = ($request->edit_access || $request->edit_access == "0")?1:0;
        $role_access->read_access = ($request->read_access || $request->read_access == "0")?1:0;
        $role_access->delete_access = ($request->delete_access || $request->delete_access == "0")?1:0;
        $role_access->updated_by = Auth::user()->name;
        $role_access->updated_at = new \DateTime;
        
        $role_access->save();
        // dd($this->route_parent, $role_access->idr_role);
        return redirect(url($this->route_parent, $role_access->idr_role))->with(['success' => 'Data berhasil diperbarui!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = $this->role_access->findOrFail($id);
        if($delete->delete()){
            return response()->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()->json(['code' => '400', 'message' => 'Data gagal dihapus']);
        }
    }
}
