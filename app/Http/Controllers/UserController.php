<?php

namespace App\Http\Controllers;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    protected $route_name = 'user';

    public function __construct(User $user)
    {
        $this->middleware('auth');
        $this->user = $user;
        
        $var = array(
            'title' => 'User',
            'route_name' => 'user'
        );

        $breadcrumb = [
            [
                "route" => 'home',
                "name" => "Home"
            ],
            [
                "route" => 'user',
                "name" => "User"
            ]
        ];

        View::share(['var'=> $var, 'breadcrumb' => $breadcrumb]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->user->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('name',function($data){
                    if (empty($data->name)) {
                        $name = "<p class='text-danger'>Tidak ada Nama</p>";
                    } else {
                        $name = $data->name;
                    }
                    return $name;
                })

                ->addColumn('email',function($data){
                    if (empty($data->email)) {
                        $email = "<p class='text-danger'>Tidak ada Email</p>";
                    } else {
                        $email = $data->email;
                    }
                    return $email;
                })
                
                ->addColumn('action', function($data){
                    $btn = '
                        <a href="'.route($this->route_name.'.edit',$data->id).'" class="btn btn-icon btn-warning my-1" title="Edit"><i class="fas fa-edit"></i></a>
                        <a href="'.route($this->route_name.'.destroy', $data->id).'" data-id="" class="btn btn-icon btn-danger btn-delete-on-table" title="Delete"><i class="fas fa-trash"></i></a>
                        ';
                    return $btn;
                })
                ->rawColumns(['action','name','email'])
                ->make(true);
        }
        return view($this->route_name.'.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = route($this->route_name.".store");
        
        return view($this->route_name.'.form', compact('action'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required',
        ],[
            'required'  => 'Kolom :attribute tidak boleh kosong.',
            'unique'    => ':attribute sudah digunakan'
        ]);
        
        $user = new $this->user;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = md5('user');
        // $user->created_by = Auth::user()->name;
        $user->created_at = new \DateTime;

        $user->save();
        return redirect($this->route_name)->with(['success' => 'Data berhasil disimpan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $action = route($this->route_name.'.update',$id);
        $data = $this->user->findOrFail($id);
        return view($this->route_name.'.form', compact('data', 'action'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required',
        ],[
            'required'  => 'Kolom :attribute tidak boleh kosong.',
            'unique'    => ':attribute sudah digunakan'
        ]);
        
        $user = $this->user->findOrFail($id);

        $user->name = $request->name;
        $user->email = $request->email;
        // $user->updated_by = Auth::user()->name;
        $user->updated_at = new \DateTime;

        $user->save();
        return redirect($this->route_name)->with(['success' => 'Data berhasil diperbarui!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = $this->user->findOrFail($id);
        if($delete->delete()){
            return response()->json(['code' => '200', 'message' => 'Data berhasil dihapus']);
        }else{
            return response()->json(['code' => '400', 'message' => 'Data gagal dihapus']);
        }
    }
}
