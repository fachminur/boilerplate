<?php

use Illuminate\Support\Facades\Auth;
use App\Models\Rrole;
use App\Models\Rmenu;

if(!function_exists("removeWhiteSpace")){
    function removeWhiteSpace($string){
        return strtolower(str_replace(" ", "-", $string));
    }
}

if(!function_exists('getPermissionMenu')){
    function getPermissionMenu($kdMenu){
        $data = Rmenu::leftJoin('t_role_access', function($join) {
            $join->on('t_role_access.idr_menu', '=', 'r_menu.idr_menu');
          })
        ->where('kode_menu', $kdMenu)
        ->where('status_menu', '1')
        ->firstOrFail([
            'r_menu.idr_menu',
            'idr_parent',
            'kode_menu',
            'name_menu',
            'position_menu',
            'controller_menu',
            'status_menu',
            'idr_role',
            'read_access',
            'add_access',
            'edit_access',
            'delete_access'
        ]);
        return $data;
    }
}

if(!function_exists('getRoleName')){
    function getRoleName($idrRole){
        $data = Rrole::where('idr_role', $idrRole)->first();
        return $data['role_name'];
    }
}