<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TuserRole extends Model
{
    use HasFactory;

    protected $table = 't_user_role';
    protected $primaryKey = 'idt_user_role';

    protected $fillable = [
        'idt_user',
        'idr_role',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
}
