<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rrole extends Model
{
    use HasFactory;

    protected $table = 'r_role';
    protected $primaryKey = 'idr_role';

    protected $fillable = [
        'role_name',
        'role_code',
        'role_status',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];
}
