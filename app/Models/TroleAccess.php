<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TroleAccess extends Model
{
    use HasFactory;

    protected $table = 't_role_access';
    protected $primaryKey = 'idt_role_access';

    protected $fillable = [
        'idr_role',
        'idr_menu',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public function role() {
        return $this->belongsTo('App\Models\Rrole', 'idr_role', 'idr_role');
    }

    public function menu() {
        return $this->belongsTo('App\Models\Rmenu', 'idr_menu', 'idr_menu');
    }
}
