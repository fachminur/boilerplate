<!-- Main Layout -->
@extends('layouts.main')
<!-- Additional css Content -->
@section('style')
    @include('partials.css');
@endsection

@section('content')
<style>
    table td, table tr th {
        vertical-align: middle !important;
    }
</style>
<!-- Main Content -->
<section class="section">
    @include('partials.section-header')

    <div class="section-body">
        <h2 class="section-title">{{ $var['title'] }}</h2>
        <p class="section-lead">Components that can be used to make something bigger than the header.</p>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Menu Tables</h4>
                        {{-- jika permission create true --}}
                        @if ($permissionMenu['add_access'] == '1')
                            <div style="display: block; float: right; margin-left: auto;">
                                <a href="{{ route('menu.create') }}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-plus"></i> Add</a>
                            </div>
                        @endif
                        {{-- end of jika permission create true --}}
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-responsive-sm display" id="table-1" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Parent</th>
                                    <th>Route</th>
                                    <th>Icon</th>
                                    <th>Order</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
{{-- Additional JS --}}
@include('partials.js')

<script>
var table;
table = $("#table-1").dataTable({
    processing: true,
    serverSide: true,
    ajax: "{{ route('menu.index') }}",
    columns: [
        {data: 'DT_RowIndex', className : "text-center"},
        {data: 'name_menu'},
        {data: 'position_menu'},
        {data: 'parent'},
        {data: 'route'},
        {data: 'icon'},
        {data: 'order_menu', className : "text-center"},
        {data: 'status', className : "text-center"},
        {data: 'action', className : "text-center", orderable: false, searchable: false},
    ]
});

$("tbody").on("click",".btn-update-on-table",function(e){
    e.preventDefault();
    var thisUrl = $(this).attr("href");
    var data = $("#table-1").DataTable().row($(this).parents('tr')).data();

    var array = {
        'id' : data.idr_menu,
        'value' : data.order_menu
    }
    console.log(array);

    Swal.fire({
        title: 'Tunggu sesaat...',
        text: 'Aplikasi sedang melakukan proses data',
        closeOnClickOutside: false,
        allowOutsideClick: false
    });
    Swal.showLoading();
    $.ajax({
        url : thisUrl,
        type : "POST",
        dataType : "json",
        data: array,
        success : function(result){
            if(result.code == 200){
                swal.fire("Success", result.message,"success");
            //   table.ajax.reload( null, false );
            $("#table-1").DataTable().ajax.reload();
            }else if(result.code == 400){
                swal.fire("Oops!", result.message,"error");
            }else{
                swal.fire("Oops!","Gagal memproses data, silahkan coba lagi", 'error');
            }
        },
        error : function()
        {
            swal.fire("Oops!","Gagal memproses data, silahkan coba lagi", 'error');
        }
    })
});
</script>
@endsection
