<!-- Main Layout -->
@extends('layouts.main')

@section('content')
<section class="section">
    <div class="section-body">
        <div class="card">
            <form method="POST" action="{{ $action }}">
                @csrf
                @if (isset($data))
                    @method('PUT')
                @endif
                <div class="card-header">
                    <h4>Form {{ isset($data)?'Ubah':'Tambah' }} {{ $var['title'] }}</h4>
                </div>
                <div class="card-body">
                    <div>
                        <div class="form-group">
                            <label>Posisi Menu</label>
                            <select name="position_menu" class="form-control select2 @error('position_menu') is-invalid @enderror" aria-required="true" aria-invalid="false">
                              <option value="">-- Pilih Data --</option>
                              @foreach ($position as $rposition)
                                  <option value="{{ $rposition }}" {{ old('position_menu', isset($data) ? $data->position_menu : '') == $rposition ? 'selected' : '' }}>{{ $rposition }}</option>
                              @endforeach
                            </select>
                          </div>
                        @error('position_menu')
                            <p style="width: 100%;margin-top: 0.25rem;font-size: 80%;color: #e3342f;">{{ $message }}</p>
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div>
                        <div class="form-group">
                            <label>Parent Menu</label>
                            <select name="idr_parent" class="form-control select2 @error('idr_parent') is-invalid @enderror" aria-required="true" aria-invalid="false">
                              <option value="">-- Pilih Data --</option>
                              @foreach ($menu as $rmenu)
                                  <option value="{{ $rmenu->idr_menu }}" {{ old('idr_parent', isset($data) ? $data->idr_parent : '') == $rmenu->idr_menu ? 'selected' : '' }}>{{ $rmenu->name_menu }}</option>
                              @endforeach
                            </select>
                          </div>
                        @error('idr_parent')
                            <p style="width: 100%;margin-top: 0.25rem;font-size: 80%;color: #e3342f;">{{ $message }}</p>
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Nama Menu</label>
                        <input type="text" name="name_menu" class="form-control @error('name_menu') is-invalid @enderror" placeholder="Contoh: Menu 1" value="{{ old('name_menu', isset($data) ? $data->name_menu : '') }}">
                        @error('name_menu')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Route Menu</label>
                        <input type="text" name="controller_menu" class="form-control @error('controller_menu') is-invalid @enderror" placeholder="Contoh : menu1" value="{{ old('controller_menu', isset($data) ? $data->controller_menu : '') }}">
                        @error('controller_menu')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Kode Menu</label>
                        <input type="text" name="kode_menu" class="form-control @error('kode_menu') is-invalid @enderror" placeholder="Contoh : MNU1" value="{{ old('kode_menu', isset($data) ? $data->kode_menu : '') }}">
                        @error('kode_menu')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Icon Menu</label>
                        <input type="text" name="icon_menu" class="form-control" placeholder="Contoh : fa-home" value="{{ isset($data) ? $data->icon_menu : old('icon_menu') }}">
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <div class="row">
                            <div class="selectgroup w-100 col-md-6">
                                <label class="selectgroup-item">
                                    <input type="radio" name="status" id="status_aktif" value="1" class="selectgroup-input" {{ old('status', isset($data) ? $data->status_menu : '') == 1 ? 'checked' : '' }}>
                                    <span class="selectgroup-button">Active</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="radio" name="status" id="status_tidakaktif" value="0" class="selectgroup-input" {{ old('status', isset($data) ? $data->status_menu : '') == 0 ? 'checked' : '' }}>
                                    <span class="selectgroup-button">Inactive</span>
                                </label>
                            </div>
                        </div>
                        <div class="invalid-feedback">
                            Good job!
                        </div>
                </div>
                <div class="card-footer text-right">
                    <a href="{{ route('menu.index') }}" class="btn btn-info icon-left"><i class="fas fa-arrow-left mr-1"></i>Back</a>
                    <button class="btn btn-primary"><i class="far fa-save mr-1"></i>Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
@section('script')
@include('partials.js_form')
@endsection