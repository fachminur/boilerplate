<!-- Main Layout -->
@extends('layouts.main')
<!-- Additional css Content -->
@section('style')
    @include('partials.css');
@endsection

@section('content')
<style>
    table th {
        text-align: center !important;
    }
    table td, table th {
        vertical-align: middle !important;
    }
</style>
<!-- Main Content -->
<section class="section">
    @include('partials.section-header')
    
    <div class="section-body">
        <h2 class="section-title">{{ $var['title'] }}</h2>
        <p class="section-lead">Components that can be used to make something bigger than the header.</p>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>{{ $var['title'] }} DataTables</h4>
                        <div style="display: block; float: right; margin-left: auto;">
                            <a href="{{ url('role/detail-role/create', $idr_role) }}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-plus"></i> Add</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-responsive-sm" id="table-1">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Role</th>
                                    <th>Menu Access</th>
                                    <th>Read</th>
                                    <th>Add</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
{{-- Additional JS --}}
@include('partials.js')

<script>
 var table = $("#table-1").dataTable({
    processing: true,
    serverSide: true,
    // ajax: "{{ route($var['route_name'].'.index') }}",
    ajax: "{{ url('role/detail-role', $idr_role) }}",
    columns: [
        {data: 'DT_RowIndex', className : "text-center"},
        {data: 'role'},
        {data: 'access'},
        {data: 'read', className : "text-center"},
        {data: 'add', className : "text-center"},
        {data: 'edit', className : "text-center"},
        {data: 'delete', className : "text-center"},
        {data: 'action', className : "text-center", orderable: false, searchable: false},
    ]
});
</script>
@endsection
