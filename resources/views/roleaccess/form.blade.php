<!-- Main Layout -->
@extends('layouts.main')

@section('content')
<section class="section">
    <div class="section-body">
        <div class="card">
            <form method="POST" action="{{ $action }}">
                @csrf
                @if (isset($data))
                    @method('PUT')
                @endif
                
                <div class="card-header">
                    <h4>{{ isset($data)?'Ubah':'Tambah' }} {{ $var['title'] }}</h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Role</label>
                        <input type="hidden" value="{{ $idr_role }}" name="role">
                        <select name="role" class="form-control select2 @error('role') is-invalid @enderror" aria-required="true" aria-invalid="false" disabled>
                          <option value="">-- Pilih Data --</option>
                          @foreach ($role as $rrole)
                          @if (isset($data))
                            <option value="{{ $rrole->idr_role }}" 
                            @if ($rrole->idr_role == $data->idr_role) selected @endif
                            >{{ $rrole->role_name }}</option>
                          @else
                            <option value="{{ $rrole->idr_role }}" 
                                @if ($idr_role == $rrole->idr_role) selected @endif
                                >{{ $rrole->role_name }}</option>
                          @endif
                          @endforeach
                        </select>
                        @error('role')
                            <p style="width: 100%;font-size: 80%;color: #e3342f;">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label>Menu</label>
                        <select name="menu" class="form-control select2 @error('menu') is-invalid @enderror" aria-required="true" aria-invalid="false" @if (isset($data)) disabled @endif>
                          <option value="">-- Pilih Data --</option>
                          @foreach ($menu as $rmenu)
                              <option value="{{ $rmenu->idr_menu }}" @if (isset($data)) @if ($data->idr_menu == $rmenu->idr_menu) selected @endif @endif>{{ $rmenu->name_menu }}</option>
                          @endforeach
                        </select>
                        @error('menu')
                            <p style="width: 100%;font-size: 80%;color: #e3342f;">{{ $message }}</p>
                        @enderror
                    </div>
                      
                    <div class="form-group">
                        <label>Menu Access</label>
                            <div class="row">
                                <div class="selectgroup w-100 col-md-12">
                                    <label class="selectgroup-item">
                                        <input type="checkbox" name="read_access" value="{{ isset($data)?$data->read_access:1 }}" class="selectgroup-input" @if (isset($data)) @if ($data->read_access == 1) checked @endif @endif>
                                        <span class="selectgroup-button">View</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="checkbox" name="add_access" value="{{ isset($data)?$data->add_access:1 }}" class="selectgroup-input" @if (isset($data)) @if ($data->add_access == 1) checked @endif @endif>
                                        <span class="selectgroup-button">Add</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="checkbox" name="edit_access" value="{{ isset($data)?$data->edit_access:1 }}" class="selectgroup-input" @if (isset($data)) @if ($data->edit_access == 1) checked @endif @endif>
                                        <span class="selectgroup-button">Edit</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="checkbox" name="delete_access" value="{{ isset($data)?$data->delete_access:1 }}" class="selectgroup-input" @if (isset($data)) @if ($data->delete_access == 1) checked @endif @endif>
                                        <span class="selectgroup-button">Delete</span>
                                    </label>

                                </div>
                            </div>
                        {{-- <div class="row">
                            <div class="col-md-3">
                                @foreach ($menu as $rmenu)
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="view{{ $rmenu->name_menu }}" name="read_access[]" value="" @if (isset($data)) @if ($data->add_access == $rmenu->add_access && $data->idr_menu == $rmenu->idr_menu) checked @endif @endif>
                                        <label class="custom-control-label" for="view{{ $rmenu->name_menu }}">View {{ $rmenu->name_menu }}</label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-md-3">
                                @foreach ($menu as $rmenu)
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="add{{ $rmenu->name_menu }}" name="add_access[]">
                                        <label class="custom-control-label" for="add{{ $rmenu->name_menu }}">Add {{ $rmenu->name_menu }}</label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-md-3">
                                @foreach ($menu as $rmenu)
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="edit{{ $rmenu->name_menu }}" name="edit_access[]">
                                        <label class="custom-control-label" for="edit{{ $rmenu->name_menu }}">Edit {{ $rmenu->name_menu }}</label>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-md-3">
                                @foreach ($menu as $rmenu)
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="delete{{ $rmenu->name_menu }}" name="delete_access[]">
                                        <label class="custom-control-label text-danger" for="delete{{ $rmenu->name_menu }}">Delete {{ $rmenu->name_menu }}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div> --}}
                        
                    </div>
                    
                </div>
                <div class="card-footer text-right">
                    <a href="{{ url($back_btn) }}" class="btn btn-info icon-left"><i class="fas fa-arrow-left mr-1"></i>Back</a>
                    <button class="btn btn-primary"><i class="far fa-save mr-1"></i>Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection