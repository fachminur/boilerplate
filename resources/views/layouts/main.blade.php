<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>{{ config('app.name', 'Laravel') }}</title>
  <link rel="shortcut icon" href="https://getstisla.com/landing/stisla.png">
  <!-- General CSS Files -->
  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
  <!-- Ionicons -->
  <link href="//fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
  <link href="{{ asset('assets/css/@fortawesome/fontawesome-free/css/all.css') }}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="{{ asset('assets/css/iziToast.min.css') }}">
  {{-- <link href="{{ asset('assets/css/sweetalert.css') }}" rel="stylesheet" type="text/css"/> --}}
  <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
  <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
  <!-- CSS Libraries -->
  @yield('style')
  <!-- Template CSS -->
  {{-- <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">--}}
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <link rel="stylesheet" href="{{ asset('web/css/components.css') }}">
</head>
<body>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      @include('partials.navbar')
      @include('partials.sidebar')
      <!-- Main Content -->
      <div class="main-content">
          @yield('content')
        {{-- <section class="section">
          <div class="section-header">
            <h1>Blank Page</h1>
          </div>

          <div class="section-body">
          </div>
        </section> --}}
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://getstisla.com/">Stisla</a>
        </div>
        <div class="footer-right">
          2.3.0
        </div>
      </footer>
    </div>
  </div>
  <!-- General JS Scripts -->
  <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/js/popper.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
  {{-- <script src="{{ asset('assets/js/sweetalert.min.js') }}"></script> --}}
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
  <script src="{{ asset('assets/js/iziToast.min.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
  <!-- JS Libraies -->
  
  <!-- Template JS File -->
  <script src="{{ asset('web/js/stisla.js') }}"></script>
  <script src="{{ asset('web/js/scripts.js') }}"></script>
  {{-- <script src="{{ asset('web/js/custom.js') }}"></script> --}}
  <!-- Page Specific JS File -->
  @yield('script')
  <script>
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  // Fungsi delete data dari datatable
  $("tbody").on("click",".btn-delete-on-table",function(e){
    e.preventDefault();
    var thisUrl = $(this).attr("href");
    Swal.fire({
        title: "Delete Record",
        text: "Are you sure want to delete this record?",
        type: "info",
        icon: "warning",
        showDenyButton: true,
        confirmButtonText: `Yes`,
        denyButtonText: `No`,
    }).then((result) => {
    /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        $.ajax({
            url : thisUrl,
            type : "DELETE",
            dataType : "json",
            success : function(result){
                if(result.code == 200){
                  swal.fire("Success", result.message,"success");
                //   table.ajax.reload( null, false );
                $("#table-1").DataTable().ajax.reload();
                }else if(result.code == 400){
                  swal.fire("Oops!", result.message,"error");
                }else{
                  swal.fire("Oops!","Gagal memproses data, silahkan coba lagi", 'error');
                }
            },
            error : function()
            {
              swal.fire("Oops!","Gagal memproses data, silahkan coba lagi", 'error');
            }
        })
      }
    })
  });

  //fungsi menampilkan seweetaler 
  @if(session()-> has('success'))
    Swal.fire('Success!','{{ session('success') }}', 'success'); 
  @elseif(session()-> has('error'))
    Swal.fire( 'Oops!','{{ session('error') }}', 'warning'); 
  @endif
  </script>
</body>
</html>
