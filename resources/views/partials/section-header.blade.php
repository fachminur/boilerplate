<div class="section-header">
    <h1>{{ $var['title'] }}</h1>
    <div class="section-header-breadcrumb">
        @foreach ($breadcrumb as $res)
            <div class="breadcrumb-item"><a href={{ url($res['route']) }}>{{ $res['name'] }}</a></div>
        @endforeach
    </div>
</div>