<!-- Main Layout -->
@extends('layouts.main')

@section('content')
<section class="section">
    <div class="section-body">
        <div class="card">
            <form method="POST" action="{{ $action }}">
                @csrf
                @if (isset($data))
                    @method('PUT')
                @endif
                
                <div class="card-header">
                    <h4>Form {{ isset($data)?'Ubah':'Tambah' }} {{ $var['title'] }}</h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" required="" value="{{ isset($data)?$data->name:'' }}" placeholder="Contoh: Perwakilan">
                        @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" value="{{ isset($data)?$data->email:'' }}" class="form-control @error('email') is-invalid @enderror" required="" placeholder="Contoh: perwakilan@mail.com">
                        @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    
                </div>
                <div class="card-footer text-right">
                    <a href="{{ route($var['route_name'].'.index') }}" class="btn btn-info icon-left"><i class="fas fa-arrow-left mr-1"></i>Back</a>
                    <button class="btn btn-primary"><i class="far fa-save mr-1"></i>Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection