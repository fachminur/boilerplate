<!-- Main Layout -->
@extends('layouts.main')

@section('content')
<section class="section">
    <div class="section-body">
        <div class="card">
            <form method="POST" action="{{ $action }}">
                @csrf
                @if (isset($data))
                    @method('PUT')
                @endif
                
                <div class="card-header">
                    <h4>
                        Form {{ isset($data)?'Ubah':'Tambah' }} {{ $var['title'] }}
                    </h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Nama Role</label>
                        <input type="text" name="role_name" class="form-control @error('role_name') is-invalid @enderror" value="{{ isset($data)?$data->role_name:'' }}" placeholder="Contoh: Perwakilan">
                        @error('role_name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Kode Role</label>
                        <input type="text" name="role_code" value="{{ isset($data)?$data->role_code:'' }}" class="form-control @error('role_code') is-invalid @enderror" placeholder="Contoh: KDPERWAKILAN">
                        @error('role_code')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <div class="row">
                            <div class="selectgroup w-100 col-md-4">
                                <label class="selectgroup-item">
                                    <input type="radio" name="role_status" id="status_aktif" value="1" class="selectgroup-input" @if (isset($data)) @if ($data->role_status == 1) checked @endif @endif>
                                    <span class="selectgroup-button">Active</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="radio" name="role_status" id="status_tidakaktif" value="0" class="selectgroup-input" @if (isset($data)) @if ($data->role_status == 0) checked @endif @endif>
                                    <span class="selectgroup-button">Inactive</span>
                                </label>
                            </div>
                        </div>
                        @error('role_status')
                            <p style="width: 100%;font-size: 80%;color: #e3342f;">{{ $message }}</p>
                        @enderror
                    </div>
                    
                </div>
                <div class="card-footer text-right">
                    <a href="{{ route('role.index') }}" class="btn btn-info icon-left"><i class="fas fa-arrow-left mr-1"></i>Back</a>
                    <button class="btn btn-primary"><i class="far fa-save mr-1"></i>Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection