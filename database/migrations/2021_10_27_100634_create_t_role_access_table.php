<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTRoleAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_role_access', function (Blueprint $table) {
            $table->id('idt_role_access');
            $table->foreignId('idr_role');
            $table->foreignId('idr_menu');
            $table->integer('read_access');
            $table->integer('add_access');
            $table->integer('edit_access');
            $table->integer('delete_access');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
            $table->foreign('idr_role')->references('idr_role')->on('r_role')->onDelete('cascade');
            $table->foreign('idr_menu')->references('idr_menu')->on('r_menu')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_role_access');
    }
}
