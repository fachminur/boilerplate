<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TroleAccess;

class TroleAccessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $access = [
            [
                'idr_role' => '1',
                'idr_menu' => '1',
                'read_access' => '1',
                'add_access' => '1',
                'edit_access' => '1',
                'delete_access' => '1',
                'created_by' => 'SEEDER',
                'created_at' => new \DateTime,
            ],
            [
                'idr_role' => '1',
                'idr_menu' => '2',
                'read_access' => '1',
                'add_access' => '1',
                'edit_access' => '1',
                'delete_access' => '1',
                'created_by' => 'SEEDER',
                'created_at' => new \DateTime,
            ],
            [
                'idr_role' => '1',
                'idr_menu' => '3',
                'read_access' => '1',
                'add_access' => '1',
                'edit_access' => '1',
                'delete_access' => '1',
                'created_by' => 'SEEDER',
                'created_at' => new \DateTime,
            ],
            [
                'idr_role' => '1',
                'idr_menu' => '4',
                'read_access' => '1',
                'add_access' => '1',
                'edit_access' => '1',
                'delete_access' => '1',
                'created_by' => 'SEEDER',
                'created_at' => new \DateTime,
            ],
            [
                'idr_role' => '1',
                'idr_menu' => '5',
                'read_access' => '1',
                'add_access' => '1',
                'edit_access' => '1',
                'delete_access' => '1',
                'created_by' => 'SEEDER',
                'created_at' => new \DateTime,
            ],
            [
                'idr_role' => '2',
                'idr_menu' => '1',
                'read_access' => '1',
                'add_access' => '0',
                'edit_access' => '0',
                'delete_access' => '0',
                'created_by' => 'SEEDER',
                'created_at' => new \DateTime,
            ],
            [
                'idr_role' => '2',
                'idr_menu' => '2',
                'read_access' => '1',
                'add_access' => '0',
                'edit_access' => '0',
                'delete_access' => '0',
                'created_by' => 'SEEDER',
                'created_at' => new \DateTime,
            ],
        ];

        TroleAccess::insert($access);
    }
}
