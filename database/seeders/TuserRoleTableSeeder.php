<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TuserRole;

class TuserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = [
            [
                'idt_user' => '1',
                'idr_role' => '1',                
                'created_by' => 'SEEDER',
                'created_at' => new \DateTime,
            ],
            [
                'idt_user' => '2',
                'idr_role' => '2',                
                'created_by' => 'SEEDER',
                'created_at' => new \DateTime,
            ],
        ];

        TuserRole::insert($role);
    }
}
