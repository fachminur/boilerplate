<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Rrole;

class RroleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = [
            [
                'role_name' => 'Admin',                
                'role_code' => 'KDADMIN',
                'role_status' => 1,
                'created_by' => 'SEEDER',
                'created_at' => new \DateTime,
            ],
            [
                'role_name' => 'Guest',                
                'role_code' => 'KDGUEST',
                'role_status' => 1,
                'created_by' => 'SEEDER',
                'created_at' => new \DateTime,
            ],
        ];

        Rrole::insert($role);
    }
}
