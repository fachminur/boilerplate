<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'Administrator',                
                'email' => 'admin@mail.com',
                'idr_role' => '1',
                'password' => bcrypt('admin123'),
                'created_at' => new \DateTime,
            ],
            [
                'name' => 'Guest',                
                'email' => 'guest@mail.com',
                'idr_role' => '2',
                'password' => bcrypt('guest123'),
                'created_at' => new \DateTime,
            ],
        ];

        User::insert($user);
    }
}
