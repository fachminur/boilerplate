<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\RoleAccessController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    // MENU
    Route::resource('menu', MenuController::class);
    Route::post('/menuup', [MenuController::class, 'up'])->name('menu.up');
    Route::post('/menudown', [MenuController::class, 'down'])->name('menu.down');

    // ROLE
    Route::resource('role', RoleController::class);
    
    // ROLE ACCESS
    Route::resource('roleaccess', RoleAccessController::class);
    Route::get('/role/detail-role/{id}', [RoleAccessController::class, 'detailRole']); 
    Route::get('/role/detail-role/create/{id}', [RoleAccessController::class, 'create']); 
    Route::post('/role/detail-role/store/{id}', [RoleAccessController::class, 'store']); 
    Route::get('/role/detail-role/edit/{id}', [RoleAccessController::class, 'edit']); 
    Route::put('/role/detail-role/update/{id}', [RoleAccessController::class, 'update']); 
    Route::delete('/role/detail-role/destroy/{id}', [RoleAccessController::class, 'destroy']); 

    // USER
    Route::resource('user', UserController::class);
});